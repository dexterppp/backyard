USE [2c2p]
GO
/****** Object:  Table [dbo].[Customers]    Script Date: 13/10/2019 5:13:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customers](
	[Id] [decimal](10, 0) NOT NULL,
	[Name] [nvarchar](30) NOT NULL,
	[Email] [nvarchar](25) NOT NULL,
	[MobileNo] [varchar](10) NOT NULL,
 CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Transactions]    Script Date: 13/10/2019 5:13:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transactions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CreatedDate] [datetime2](7) NOT NULL,
	[Amount] [decimal](10, 2) NOT NULL,
	[CurrencyCode] [nvarchar](3) NOT NULL,
	[CustomerId] [decimal](10, 0) NOT NULL,
	[Status] [tinyint] NOT NULL,
 CONSTRAINT [PK_Transactions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[Customers] ([Id], [Name], [Email], [MobileNo]) VALUES (CAST(123456 AS Decimal(10, 0)), N'Tomas Adisan', N'tomas@gmail.com', N'0123456789')
INSERT [dbo].[Customers] ([Id], [Name], [Email], [MobileNo]) VALUES (CAST(123457 AS Decimal(10, 0)), N'Richard	Arnold', N'rechard@gmail.com', N'0123456789')
INSERT [dbo].[Customers] ([Id], [Name], [Email], [MobileNo]) VALUES (CAST(123458 AS Decimal(10, 0)), N'Samantha	Watson', N'Samantha@mail.com', N'0123456789')
SET IDENTITY_INSERT [dbo].[Transactions] ON 

INSERT [dbo].[Transactions] ([Id], [CreatedDate], [Amount], [CurrencyCode], [CustomerId], [Status]) VALUES (3, CAST(N'2018-02-13T21:34:00.0000000' AS DateTime2), CAST(1234.56 AS Decimal(10, 2)), N'USD', CAST(123456 AS Decimal(10, 0)), 1)
INSERT [dbo].[Transactions] ([Id], [CreatedDate], [Amount], [CurrencyCode], [CustomerId], [Status]) VALUES (4, CAST(N'2018-03-13T21:34:00.0000000' AS DateTime2), CAST(12345.56 AS Decimal(10, 2)), N'USD', CAST(123457 AS Decimal(10, 0)), 1)
INSERT [dbo].[Transactions] ([Id], [CreatedDate], [Amount], [CurrencyCode], [CustomerId], [Status]) VALUES (5, CAST(N'2018-04-13T21:34:00.0000000' AS DateTime2), CAST(100.56 AS Decimal(10, 2)), N'USD', CAST(123457 AS Decimal(10, 0)), 1)
INSERT [dbo].[Transactions] ([Id], [CreatedDate], [Amount], [CurrencyCode], [CustomerId], [Status]) VALUES (6, CAST(N'2018-05-13T21:34:00.0000000' AS DateTime2), CAST(100.00 AS Decimal(10, 2)), N'USD', CAST(123457 AS Decimal(10, 0)), 1)
SET IDENTITY_INSERT [dbo].[Transactions] OFF
ALTER TABLE [dbo].[Transactions]  WITH CHECK ADD  CONSTRAINT [FK_Transactions_Customers_CustomerId] FOREIGN KEY([CustomerId])
REFERENCES [dbo].[Customers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Transactions] CHECK CONSTRAINT [FK_Transactions_Customers_CustomerId]
GO
