﻿using backyard.Domain.Models;
using backyard.Domain.Repositories;
using backyard.Domain.Services.Communication;
using backyard.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backyard.Services
{
    public class CustomerService : ICustomerService
    {
        private readonly ICustomerRepository _customerRespository;
        private readonly IUnitOfWork _unitOfWork;
        public CustomerService(ICustomerRepository customerRespository, IUnitOfWork unitOfWork)
        {
            _customerRespository = customerRespository;
            _unitOfWork = unitOfWork;
        }

        public async Task<CustomerResponse> GetAsync(RequestCustomerResource param)
        {
            if (!param.CustomerID.HasValue && string.IsNullOrEmpty(param.Email))
            {
                return new CustomerResponse("No inquiry criteria");
            }
            if (param.CustomerID.HasValue && param.CustomerID <= 0)
            {
                return new CustomerResponse("Invalid Customer ID");
            }
            if (!string.IsNullOrEmpty(param.Email) && !IsValidEmail(param.Email))
            {
                return new CustomerResponse("Invalid Email");
            }
            else
            {
                try
                {
                    var existingCustomer = await _customerRespository.FindAsync(param.CustomerID, param.Email.ToLower());
                    return new CustomerResponse(existingCustomer);
                } 
                catch (Exception ex)
                {
                    return new CustomerResponse(string.Empty);
                }
            }
        }
        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
