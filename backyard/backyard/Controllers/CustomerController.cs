﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using backyard.Domain.Models;
using backyard.Resources;
using backyard.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace backyard.Controllers
{
    [ApiController]
    [Route("/api/customers")]
    public class CustomerController : ControllerBase
    {
        private readonly ILogger<CustomerController> _logger;
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;

        public CustomerController(ILogger<CustomerController> logger, ICustomerService customerService, IMapper mapper)
        {
            _logger = logger;
            _customerService = customerService;
            _mapper = mapper;
        }

        [HttpPost]
        [ProducesResponseType(typeof(CustomerResource), 200)]
        public async Task<IActionResult> PostAsync([FromBody] RequestCustomerResource resource)
        {
            var result = await _customerService.GetAsync(resource);
            if (!result.Success)
            {
                return BadRequest(result.Message);
            }
            else if (result.Resource == null)
            {
                return NotFound();
            }
            
            var customerResource = _mapper.Map<Customer, CustomerResource>(result.Resource);
            return Ok(customerResource);
        }
    }
}
