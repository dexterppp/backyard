﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backyard.Resources
{
    public class RequestCustomerResource
    {
        [JsonProperty("customerID")]
        public int? CustomerID { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
    }
}
