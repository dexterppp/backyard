﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backyard.Resources
{
    public class CustomerResource
    {
        [JsonProperty("customerID")]
        public string CustomerID { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("mobile")]
        public string Mobile { get; set; }

        [JsonProperty("transactions")]
        public IEnumerable<TransactionResource> Transactions { get; set; }
    }
}
