﻿using AutoMapper;
using backyard.Domain.Models;
using backyard.Resources;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backyard.Mapper
{
    public class ModelToResourceProfile : Profile
    {
        public ModelToResourceProfile()
        {
            CreateMap<Customer, CustomerResource>()
                .ForMember(dest => dest.CustomerID, opt => opt.MapFrom(src => (int)src.Id))
                .ForMember(dest => dest.Mobile, opt => opt.MapFrom(src => src.MobileNo));
            CreateMap<Transaction, TransactionResource>()
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status.ToString()))
                .ForMember(dest => dest.Currency, opt => opt.MapFrom(src => src.CurrencyCode))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.CreatedDate.ToString("dd/MM/yyyy hh:mm")));
        }
    }
}
