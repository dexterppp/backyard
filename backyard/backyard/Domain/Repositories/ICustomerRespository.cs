﻿using backyard.Domain.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace backyard.Domain.Repositories
{
    public interface ICustomerRepository
    {
        Task<Customer> FindAsync(int? id, string email);
    }
}
