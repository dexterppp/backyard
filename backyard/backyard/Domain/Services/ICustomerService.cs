﻿using backyard.Domain.Services.Communication;
using backyard.Resources;
using System.Threading.Tasks;

namespace backyard.Services
{
    public interface ICustomerService
    {
        Task<CustomerResponse> GetAsync(RequestCustomerResource param);
    }
}