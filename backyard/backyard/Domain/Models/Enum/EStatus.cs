﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace backyard.Domain.Models.Enum
{
    public enum EStatus : byte
    {
        Failed,
        Success,
        Canceled
    }
}
