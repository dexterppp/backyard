﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace backyard.Domain.Models
{
    [Table("Customers")]
    public class Customer
    {
        [Key]
        [MaxLength(10)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public decimal Id { get; set; }

        [MaxLength(30)]
        [Required]
        public string Name { get; set; }

        [MaxLength(25)]
        [Required]
        public string Email { get; set; }

        [MaxLength(10)]
        [Required]
        public string MobileNo { get; set; }

        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
