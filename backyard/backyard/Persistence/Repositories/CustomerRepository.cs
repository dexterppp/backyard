﻿using backyard.Domain.Models;
using backyard.Domain.Repositories;
using backyard.Persistence.Contexts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace backyard.Persistence.Repositories
{
    public class CustomerRepository : BaseRepository, ICustomerRepository
    {
        public CustomerRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<Customer> FindAsync(int? id, string email)
        {
            IQueryable<Customer> query = _context.Customers.Include(x => x.Transactions);
            if (id.HasValue)
            {
                query = query.Where(x => x.Id == id.Value);
            }
            if (!string.IsNullOrEmpty(email))
            {
                query = query.Where(x => x.Email == email);
            }

            return await query.FirstOrDefaultAsync();
        }
    }
}
