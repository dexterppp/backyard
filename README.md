# 2C2P Backyard API

Simple RESTful API built with ASP.NET Core 3.0 to show how to create RESTful services using a decoupled, maintainable architecture.
